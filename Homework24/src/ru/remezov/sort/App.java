package ru.remezov.sort;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class App {
    public static void main(String[] args) {
        Set<String> words = new HashSet<>();

        words.add("foo");
        words.add("buzz");
        words.add("bar");
        words.add("fork");
        words.add("bort");
        words.add("spoon");
        words.add("!");
        words.add("dude");

        System.out.println(words);

        System.out.println(removeEvenLength(words));
    }

    public static Set<String> removeEvenLength(Set<String> set) {
        Iterator<String> iterator = set.iterator();
        while (iterator.hasNext()) {
            String word = iterator.next();
            if ((word.toCharArray().length % 2) == 0) {
                iterator.remove();
            }
        }
        return set;
    }
}
